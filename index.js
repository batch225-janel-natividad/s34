/*
Activity:
1. Create a GET route that will access the "/home" route that will print out a simple message.
2. Process a GET request at the "/home" route using postman.
3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
4. Process a GET request at the "/users" route using postman.
5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
6. Process a DELETE request at the "/delete-user" route using postman.
7. Export the Postman collection and save it inside the root folder of our application.
8. Create a git repository named S34.
9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
10. Add the link in Boodle.
*/

const express = require("express");

const app = express();

const port = 3001;
app.use(express.json());

app.use(express.urlencoded({extended:true}));

//1
app.get("/home", (req, res) => {
	res.send("Welcome to home page");

});

//3
let users = [];

app.post("/users-signup", (req, res) => {

	console.log(req.body);

	if(req.body.username !== ''){

		users.push(req.body)

		res.send(`User ${req.body.username} successfully registered`);
	} else {

		res.send("Please input  username");
	}
});

 app.get('/users', (req, res) => {

 	res.json(users);
 })

//DELETE
app.delete("/delete-users", (req, res) => {

	console.log(req.body);

	if(req.body.username !== ''){

		users.pop(req.body)

		res.send(`User ${req.body.username} successfully deleted`);
	} else {

		res.send("Please input  username");
	}
});

app.listen(port, () => console.log(`Server running at port ${port}`))